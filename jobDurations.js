// [...document.querySelectorAll('.ci-table.pipeline tr:not(.build.commit)')].forEach(a => a.style.display = 'none');
// [...document.querySelectorAll('.ci-table.pipeline td:nth-child(4) p')].forEach(a => a.style.display = 'inline');
// [...document.querySelectorAll('.ci-table.pipeline td')].forEach(a => a.style.padding = '0 16px');

function getIntervalFromFinishedAtElement(el) {
    const timeElement = el.querySelector('time');
    if(!timeElement) return undefined;

    const endMs = new Date(timeElement.dateTime).getTime();
    const durationEl = el.querySelector('[data-testid=job-duration]');
    const durationStr = durationEl ? durationEl.textContent.trim() : "00:00:00";
    const [hours, minutes, seconds] = durationStr.split(':').map(part => parseInt(part, 10));
    const durationMs = (hours * 60 * 60 + minutes * 60 + seconds) * 1000;
    const startMs = endMs - durationMs;
    return [startMs, endMs];
}

chrome.t2003_gitlab_plugin = chrome.t2003_gitlab_plugin ?? {};

chrome.t2003_gitlab_plugin.jobs = [...document.querySelectorAll('[data-testid=jobs-table-row]')].map(el => {
    const displayEl = el.querySelectorAll('td')[1];
    const intervalMs = getIntervalFromFinishedAtElement(el);
    return { displayEl, intervalMs };
}).filter(({ intervalMs }) => intervalMs);
chrome.t2003_gitlab_plugin.minMaxMs = [
    Math.min(...chrome.t2003_gitlab_plugin.jobs.map(a => a.intervalMs[0])),
    Math.max(...chrome.t2003_gitlab_plugin.jobs.map(a => a.intervalMs[1])),
];
chrome.t2003_gitlab_plugin.jobs.forEach(({displayEl, intervalMs}) => {
    const minMaxMs = chrome.t2003_gitlab_plugin.minMaxMs;
    const intervalPercent = [
        (((intervalMs[0] - minMaxMs[0]) / (minMaxMs[1] - minMaxMs[0])) * 100).toFixed(2) + '%',
        (((intervalMs[1] - minMaxMs[0]) / (minMaxMs[1] - minMaxMs[0])) * 100).toFixed(2) + '%',
    ];
    displayEl.style.background = `linear-gradient(90deg, #00880020 ${intervalPercent[0]}, green ${intervalPercent[0]} ${intervalPercent[1]}, #00880020 ${intervalPercent[1]})`;
});
