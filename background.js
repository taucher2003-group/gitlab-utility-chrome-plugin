chrome.contextMenus.create({
    id: "gl-extension-menu-jobs",
    title: "Show Job Durations",
    contexts: ["page"]
});

chrome.contextMenus.onClicked.addListener((info, tab) => {
    applyDurations(tab);
})

// chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
//     if (changeInfo.status === 'complete' && tab.active) {
//         applyDurations(tab);
//     }
// })

function applyDurations(tab) {
    const tabId = tab.id;
    chrome.scripting.executeScript({
        target: {tabId},
        files: ['jobDurations.js']
    }, () => {});
}